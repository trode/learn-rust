fn main() {
    let x = 5;

    let y = {
        let x = 3;
        1
    };

    println!("The value of y is: {}", y);
}